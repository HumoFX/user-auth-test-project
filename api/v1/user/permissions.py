from rest_framework.permissions import BasePermission


class IsAdmin(BasePermission):
    """
    Allows access only to admin users.
    """
    message = 'Only admin can access this page'

    def has_permission(self, request, view):
        return request.user.is_superuser

