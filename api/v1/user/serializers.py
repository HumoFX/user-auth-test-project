# serializers for user app
# from rest_framework import serializers
from rest_framework import serializers

from apps.user.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        read_only_fields = (
            'id', 'is_active', 'is_staff', 'is_superuser', 'last_login', 'date_joined', 'created_at', 'updated_at')


class UserRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'phone_number', 'password', 'first_name', 'last_name')
        read_only_fields = (
            'id', 'is_active', 'is_staff', 'is_superuser', 'last_login', 'date_joined', 'created_at', 'updated_at')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user


class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'phone_number', 'is_active', 'is_staff', 'is_superuser', 'last_login',
                  'date_joined', 'created_at', 'updated_at')
        read_only_fields = (
            'id', 'is_active', 'is_staff', 'is_superuser', 'last_login', 'date_joined', 'created_at', 'updated_at')
