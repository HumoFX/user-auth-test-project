from django.urls import path
from api.v1.user.views import UserListView, UserRegistrationView, UserLogoutView, UserDetailView
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView


urlpatterns = [
    path('registration/', UserRegistrationView.as_view(), name='user_registration'),
    path('login/', TokenObtainPairView.as_view(), name='user_login'),
    path('logout/', UserLogoutView.as_view(), name='user_logout'),
    path('refresh/', TokenRefreshView.as_view(), name='user_refresh'),
    path('list/', UserListView.as_view(), name='user_list'),
    path('detail/<int:pk>/', UserDetailView.as_view(), name='user_detail'),
]