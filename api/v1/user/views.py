from django.contrib.auth.hashers import make_password
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from api.v1.user.serializers import UserSerializer, UserListSerializer, UserRegistrationSerializer
from apps.user.models import User
from api.v1.user.permissions import IsAdmin


# views for user registration

class UserRegistrationView(CreateAPIView):
    """
    User registration view
    """
    serializer_class = UserRegistrationSerializer
    queryset = User.objects.all()


class UserLogoutView(APIView):
    """
    User logout view
    """
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()
            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class UserCreateView(CreateAPIView):
    """
    User create view
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get_permissions(self):
        if self.request.user.is_superuser:
            return []
        else:
            return [IsAdmin()]


class UserListView(ListAPIView):
    """
    User list view
    """
    serializer_class = UserListSerializer
    queryset = User.objects.all()
    permission_classes = (IsAdmin)
    filterset_fields = ['is_active', 'is_staff', 'is_superuser']

    def get(self, request, *args, **kwargs):
        serializer = UserListSerializer(self.get_queryset(), many=True)
        return Response(serializer.data)


class UserDetailView(RetrieveUpdateDestroyAPIView):
    """
    User detail view
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (IsAdmin)

    def get(self, request, *args, **kwargs):
        serializer = UserSerializer(self.queryset, many=True)
        return Response(serializer.data)
