from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.validators import UnicodeUsernameValidator


# Create your models here.

class User(AbstractUser):
    id = models.AutoField(primary_key=True)
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Обязательное поле. Не более 150 символов. Только буквы, цифры и символы @/./+/-/_ .'),
        validators=[UnicodeUsernameValidator()],
    )
    email = models.EmailField(_('email address'), unique=True)
    phone_number = models.CharField(max_length=12,
                                    validators=[RegexValidator(regex='^.{12}$',
                                                               message='Длина номера телефона должна быть 12 символов',
                                                               code='nomatch')], )
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    last_login = models.DateTimeField(blank=True, null=True)
    date_joined = models.DateTimeField(default=timezone.now)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
        ordering = ['id']
