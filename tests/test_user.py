from rest_framework import status

from api.v1.user.views import UserCreateView
from apps.user.models import User
from django.core.management import call_command


def test_create_user_with_invalid_data():
    """
    Let's try to create a user with invalid data and see if it fails.
    """
    # Arrange
    request_data = {
        'username': 'jack_sparrow',
        'email': 'qwe',
        'phone_number': '123456789012',
    }
    view = UserCreateView()

    # Act
    response = view.create(request_data)
    # Assert
    assert response.data['email'][0] == 'Enter a valid email address.'
    assert response.data['password'][0] == 'This field is required.'
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_create_user_with_valid_data(self):
    """
    Ahoy, matey! Let's create a user with valid data and see if it be successful.
    """
    # Arrange
    request_data = {
        'username': 'jack_sparrow',
        'email': 'jack@sparrow.com',
        'phone_number': '123456789012',
        'password': 'password123',
    }
    view = UserCreateView()

    # Act
    response = view.create(request_data)

    # Assert
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data['username'] == 'jack_sparrow'
    assert response.data['email'] == 'jack@sparrow.com'
    assert response.data['phone_number'] == '123456789012'
    assert response.data['password'] != 'password123'
    assert User.objects.filter(username='jack_sparrow').exists()
